Таблица Actors - содержит информацию об актерах.

    id - уникальный идентификатор актера (primary key).
    full_name - полное имя актера.
    bio - биография актера.
    contacts - контактная информация актера.


CREATE TABLE Actors (
  id INT UNSIGNED AUTO_INCREMENT,
  full_name VARCHAR(255) NOT NULL,
  bio TEXT,
  contacts VARCHAR(255),
  PRIMARY KEY(`id`)
);








Таблица Directors - содержит информацию о режиссерах.

    id - уникальный идентификатор режиссера (primary key).
    full_name - полное имя режиссера.
    bio - биография режиссера.
    main_production - главная постановка режиссера.
    contacts - контактная информация режиссера.


CREATE TABLE Directors (
  id INT UNSIGNED AUTO_INCREMENT,
  full_name VARCHAR(255) NOT NULL,
  bio TEXT,
  main_production VARCHAR(50) NOT NULL,
  contacts VARCHAR(255),
  PRIMARY KEY(`id`)
);








Таблица Costume_Designers - содержит информацию о костюмографах.

    id - уникальный идентификатор костюмографа (primary key).
    full_name - полное имя костюмографа.
    costume_example - пример костюма с описанием.
    bio - биография костюмографа.


CREATE TABLE Costume_Designers (
  id INT UNSIGNED AUTO_INCREMENT,
  full_name VARCHAR(255) NOT NULL,
  costume_example TEXT NOT NULL,
  bio TEXT NOT NULL,
  PRIMARY KEY(`id`)
);








Таблица Set_Designers - содержит информацию о художниках-декораторах.

    id - уникальный идентификатор художника-декоратора (primary key).
    full_name - полное имя художника-декоратора.
    set_example - пример декорации с описанием.
    bio - биография художника-декоратора.


CREATE TABLE Set_Designers (
  id INT UNSIGNED AUTO_INCREMENT,
  full_name VARCHAR(255) NOT NULL,
  set_example TEXT NOT NULL,
  bio TEXT NOT NULL,
  PRIMARY KEY(`id`)
);







Таблица Genres - содержит информацию о жанрах спектаклей.

    id - уникальный идентификатор жанра (primary key).
    genre_name - название жанра.
    description - описание жанра.


CREATE TABLE Genres (
  id INT UNSIGNED AUTO_INCREMENT,
  genre_name VARCHAR(50),
  description VARCHAR(255),
  PRIMARY KEY(`id`)
);










Таблица Spectacles - содержит информацию о спектаклях.

    id - уникальный идентификатор спектакля (primary key).
    genre_id - идентификатор жанра (foreign key, связь с таблицей Genres).
    director_id - идентификатор режиссера (foreign key, связь с таблицей Directors).
    costume_designer_id - идентификатор костюмографа (foreign key, связь с таблицей Costume_Designers).
    set_designer_id - идентификатор художника-декоратора (foreign key, связь с таблицей Set_Designers).
    title - название спектакля.
    author - автор спектакля.
    duration - продолжительность спектакля.
    description - описание спектакля.


CREATE TABLE Spectacles (
  id INT UNSIGNED AUTO_INCREMENT,
  genre_id INT UNSIGNED,
  director_id INT UNSIGNED,
  costume_designer_id INT UNSIGNED,
  set_designer_id INT UNSIGNED,
  title VARCHAR(255) NOT NULL,
  author VARCHAR(255) NOT NULL,
  duration TIME NOT NULL,
  description TEXT,
  PRIMARY KEY(`id`),
  CONSTRAINT fk_director FOREIGN KEY (director_id) REFERENCES Directors(id) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT fk_genre FOREIGN KEY (genre_id) REFERENCES Genres(id) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT fk_costume_designer FOREIGN KEY (costume_designer_id) REFERENCES Costume_Designers(id) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT fk_set_designer FOREIGN KEY (set_designer_id) REFERENCES Set_Designers(id) ON DELETE SET NULL ON UPDATE CASCADE
);











Таблица Roles - содержит информацию о ролях актеров в спектаклях.

    id - уникальный идентификатор роли (primary key).
    actor_id - идентификатор актера (foreign key, связь с таблицей Actors).
    spectacle_id - идентификатор спектакля (foreign key, связь с таблицей Spectacles).
    role - название роли.


CREATE TABLE Roles (
  id INT UNSIGNED AUTO_INCREMENT,
  actor_id INT UNSIGNED,
  spectacle_id INT UNSIGNED,
  role VARCHAR(20) NOT NULL,
  PRIMARY KEY(`id`),
  CONSTRAINT fk_actor FOREIGN KEY (actor_id) REFERENCES Actors(id) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT fk_spectacle_role FOREIGN KEY (spectacle_id) REFERENCES Spectacles(id) ON DELETE CASCADE ON UPDATE CASCADE
);













Таблица Scenes - содержит информацию о сценах.

    id - уникальный идентификатор сцены (primary key).
    scene_name - название сцены.
    seats_amount - количество мест в сцене.
    address - адрес сцены.
    contacts - контактная информация сцены.


CREATE TABLE Scenes (
  id INT UNSIGNED AUTO_INCREMENT,
  scene_name VARCHAR(50) NOT NULL,
  seats_amount INT NOT NULL,
  address VARCHAR(255) NOT NULL,
  contacts VARCHAR(255) NOT NULL,
  PRIMARY KEY(`id`)
);









Таблица Schedule - содержит информацию о расписании спектаклей.

    id - уникальный идентификатор записи (primary key).
    spectacle_id - идентификатор спектакля (foreign key, связь с таблицей Spectacles).
    scene_id - идентификатор сцены (foreign key, связь с таблицей Scenes).
    date - дата спектакля.
    start_time - время начала спектакля.


CREATE TABLE Schedule (
  id INT UNSIGNED AUTO_INCREMENT,
  spectacle_id INT UNSIGNED NOT NULL,
  scene_id INT UNSIGNED NOT NULL,
  date DATE NOT NULL,
  start_time TIME NOT NULL,
  PRIMARY KEY(`id`),
  CONSTRAINT fk_spectacle_schedule FOREIGN KEY (spectacle_id) REFERENCES Spectacles(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_scene_schedule FOREIGN KEY (scene_id) REFERENCES Scenes(id) ON DELETE CASCADE ON UPDATE CASCADE
);












Таблица Discounts - содержит информацию о скидках.

    id - уникальный идентификатор скидки (primary key).
    discount_name - название скидки.
    discount_percent - процент скидки.
    start_date - дата начала действия скидки.
    end_date - дата окончания действия скидки.


CREATE TABLE Discounts (
  id INT UNSIGNED AUTO_INCREMENT,
  discount_name VARCHAR(255) NOT NULL,
  discount_percent DECIMAL(8, 2) NOT NULL,
  start_date DATE NOT NULL,
  end_date DATE,
  PRIMARY KEY(`id`)
);










Таблица Users - содержит информацию о пользователях.

    id - уникальный идентификатор пользователя (primary key).
    discount_id - идентификатор скидки (foreign key, связь с таблицей Discounts).
    full_name - полное имя пользователя.
    phone_number - номер телефона пользователя.
    email - адрес электронной почты пользователя.
    status - статус пользователя.


CREATE TABLE Users (
  id INT UNSIGNED AUTO_INCREMENT,
  discount_id INT UNSIGNED,
  full_name VARCHAR(255) NOT NULL,
  phone_number VARCHAR(20),
  email VARCHAR(255),
  status ENUM('Active', 'Inactive') NOT NULL,
  PRIMARY KEY(`id`),
  CONSTRAINT fk_discount FOREIGN KEY (discount_id) REFERENCES Discounts(id) ON DELETE SET NULL ON UPDATE CASCADE
);













Таблица Places - содержит информацию о местах в сценах.

    id - уникальный идентификатор места (primary key).
    scene_id - идентификатор сцены (foreign key, связь с таблицей Scenes).
    view - вид места (например, партер, балкон).
    row - номер ряда.
    seat - номер места.
    status - статус места (например, доступно, забронировано, продано).


CREATE TABLE Places (
  id INT UNSIGNED AUTO_INCREMENT,
  scene_id INT UNSIGNED NOT NULL,
  view ENUM('Balcony', 'Orchestra', 'General', 'Open Seating'),
  `row` INT NOT NULL,
  seat INT NOT NULL,
  status ENUM('Occupied', 'Available', 'Booked'),
  PRIMARY KEY(`id`),
  CONSTRAINT fk_scene FOREIGN KEY (scene_id) REFERENCES Scenes(id) ON DELETE CASCADE ON UPDATE CASCADE
);










Таблица Tickets - содержит информацию о билетах.

    id - уникальный идентификатор билета (primary key).
    spectacle_id - идентификатор спектакля (foreign key, связь с таблицей Spectacles).
    scene_id - идентификатор сцены (foreign key, связь с таблицей Scenes).
    date - дата спектакля.
    start_time - время начала спектакля.
    view - вид места (например, партер, балкон).
    row - номер ряда.
    seat - номер места.
    price - цена билета.


CREATE TABLE Tickets (
  id INT UNSIGNED AUTO_INCREMENT,
  spectacle_id INT UNSIGNED NOT NULL,
  scene_id INT UNSIGNED NOT NULL,
  date DATE NOT NULL,
  start_time TIME NOT NULL,
  view ENUM('Balcony', 'Orchestra', 'General', 'Open Seating') NOT NULL,
  `row` INT NOT NULL,
  seat INT NOT NULL,
  price DECIMAL(8, 2) NOT NULL,
  PRIMARY KEY(`id`),
  CONSTRAINT fk_spectacle FOREIGN KEY (spectacle_id) REFERENCES Spectacles(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_scene_ticket FOREIGN KEY (scene_id) REFERENCES Scenes(id) ON DELETE CASCADE ON UPDATE CASCADE
);









Таблица Orders - содержит информацию о заказах.

    id - уникальный идентификатор заказа (primary key).
    ticket_id - идентификатор билета (foreign key, связь с таблицей Tickets).
    user_id - идентификатор пользователя (foreign key, связь с таблицей Users).
    order_date - дата и время оформления заказа.
    status - статус заказа.


CREATE TABLE Orders (
  id INT UNSIGNED AUTO_INCREMENT,
  ticket_id INT UNSIGNED,
  user_id INT UNSIGNED ,
  order_date TIMESTAMP NOT NULL,
  status ENUM('Confirmed', 'Pending'),
  PRIMARY KEY(`id`),
  CONSTRAINT fk_tickets FOREIGN KEY (ticket_id) REFERENCES Tickets(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES Users(id) ON DELETE SET NULL ON UPDATE CASCADE
);









Таблица Payments - содержит информацию о платежах.

    id - уникальный идентификатор платежа (primary key).
    order_id - идентификатор заказа (foreign key, связь с таблицей Orders).
    payment_method - метод оплаты.
    status - статус платежа.


CREATE TABLE Payments (
  id INT UNSIGNED AUTO_INCREMENT,
  order_id INT UNSIGNED NOT NULL,
  payment_method VARCHAR(50) NOT NULL,
  status ENUM('Completed', 'Pending') NOT NULL,
  PRIMARY KEY(`id`),
  CONSTRAINT fk_order FOREIGN KEY (order_id) REFERENCES Orders(id) ON DELETE CASCADE ON UPDATE CASCADE
);









Таблица Reviews - содержит информацию о отзывах о спектаклях.

    id - уникальный идентификатор отзыва (primary key).
    spectacle_id - идентификатор спектакля (foreign key, связь с таблицей Spectacles).
    user_id - идентификатор пользователя (foreign key, связь с таблицей Users).
    review_date - дата отзыва.
    review_text - текст отзыва.
    rating - рейтинг спектакля (от 1 до 5).


CREATE TABLE Reviews (
  id INT UNSIGNED AUTO_INCREMENT,
  spectacle_id INT UNSIGNED NOT NULL,
  user_id INT UNSIGNED NOT NULL,
  review_date DATE NOT NULL,
  review_text TEXT,
  rating INT NOT NULL,
  PRIMARY KEY(`id`),
  CONSTRAINT fk_spectacle_review FOREIGN KEY (spectacle_id) REFERENCES Spectacles(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_user_review FOREIGN KEY (user_id) REFERENCES Users(id) ON DELETE CASCADE ON UPDATE CASCADE
);



