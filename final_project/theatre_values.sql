INSERT INTO Actors (full_name, bio, contacts)
VALUES
  ('John Smith', 'John Smith is a talented actor with a versatile range of performances.', 'johnsmith@example.com'),
  ('Emma Johnson', 'Emma Johnson is an award-winning actress known for her powerful portrayals.', 'emmajohnson@example.com'),
  ('Michael Brown', 'Michael Brown has years of experience in theater and has worked in various stage productions.', 'michaelbrown@example.com'),
  ('Sophia Davis', 'Sophia Davis is a rising star in the acting industry, known for her charisma and stage presence.', 'sophiadavis@example.com'),
  ('Robert Wilson', 'Robert Wilson is a seasoned actor with a deep understanding of character development.', 'robertwilson@example.com'),
  ('Olivia Thompson', 'Olivia Thompson is a versatile actress who excels in both comedic and dramatic roles.', 'oliviathompson@example.com'),
  ('David Rodriguez', 'David Rodriguez is known for his intense and emotionally charged performances on stage.', 'davidrodriguez@example.com'),
  ('Sophie Anderson', 'Sophie Anderson brings grace and elegance to every character she portrays.', 'sophieanderson@example.com'),
  ('Matthew Lee', 'Matthew Lee is a skilled actor with a knack for physical comedy.', 'matthewlee@example.com'),
  ('Emily Moore', 'Emily Moore captivates audiences with her powerful voice and stage presence.', 'emilymoore@example.com'),
  ('Daniel Harris', 'Daniel Harris is a dedicated actor who brings depth and authenticity to his roles.', 'danielharris@example.com');

select * from actors;

INSERT INTO Directors (full_name, bio, main_production, contacts)
VALUES
  ('Jennifer Adams', 'Jennifer Adams is an accomplished director with a strong vision and innovative storytelling techniques.', 'Hamlet', 'jenniferadams@example.com'),
  ('Michael Roberts', 'Michael Roberts has directed numerous critically acclaimed productions and is known for his attention to detail.', 'The Comedy of Errors', 'michaelroberts@example.com'),
  ('Sarah Thompson', 'Sarah Thompson brings a unique and fresh perspective to her directorial work, pushing boundaries and challenging conventions.', 'Wicked', 'sarahthompson@example.com'),
  ('Jonathan Davis', 'Jonathan Davis is a visionary director who has a knack for creating visually stunning and emotionally impactful productions.', 'The Phantom of the Opera', 'jonathandavis@example.com'),
  ('Sophie Wilson', "Sophie Wilson's directing style is characterized by her ability to bring out authentic performances from actors.", 'Romeo and Juliet', 'sophiewilson@example.com');
  

INSERT INTO Costume_Designers (full_name, costume_example, bio)
VALUES
  ('Rachel Smith', 'Elegant Victorian dress with lace details. The costume is made of silk and features intricate embroidery on the bodice. It reflects the fashion of the 19th century.', 'Rachel Smith is a talented costume designer with a keen eye for detail and a passion for historical accuracy.'),
  ('Michael Johnson', 'Colorful and vibrant costume inspired by traditional folk attire. It features intricate patterns and embellishments, showcasing the rich cultural heritage of the region.', 'Michael Johnson has designed costumes for numerous theater productions, bringing characters to life through his creative designs.'),
  ('Sophie Davis', 'Contemporary costume with a futuristic twist. The design combines modern materials and unconventional silhouettes to create a bold and avant-garde look.', 'Sophie Davis combines her love for fashion and theater to create unique and visually stunning costumes for the stage.'),
  ('Daniel Wilson', 'Elaborate period costume from the Renaissance era. The design incorporates luxurious fabrics, such as velvet and brocade, and intricate embellishments, reflecting the opulence of the time.', "Daniel Wilson's costume designs are known for their intricate details and ability to capture the essence of each character."),
  ('Olivia Thompson', 'Sleek and elegant costume inspired by the fashion of the 1920s. The design features Art Deco motifs, fringe details, and a drop-waist silhouette, capturing the glamour of the era.', "Olivia Thompson's costume designs reflect her deep understanding of character psychology and storytelling.");


INSERT INTO Set_Designers (full_name, set_example, bio)
VALUES
  ('John Adams', 'A grand ballroom set with towering columns and a sparkling chandelier. The set evokes a sense of opulence and sets the stage for a glamorous event.', 'John Adams is a talented set designer with a knack for creating immersive and visually stunning stage environments.'),
  ('Emma Wilson', 'A cozy and intimate living room set with a fireplace, comfortable furniture, and warm lighting. The set creates a homely atmosphere and is perfect for intimate scenes.', "Emma Wilson's set designs combine creativity and practicality to transform the stage into captivating worlds."),
  ('Michael Davis', 'A futuristic cityscape set with towering skyscrapers, holographic billboards, and advanced technology. The set transports the audience into a sci-fi world of the future.', "Michael Davis's set designs are characterized by their innovative use of space and ability to enhance storytelling."),
  ('Sophie Roberts', 'A versatile and modular set consisting of movable platforms and rotating panels. The set can be rearranged to create different locations, providing flexibility for various scenes.', 'Sophie Roberts creates sets that seamlessly blend aesthetics with functionality, providing a dynamic backdrop for performances.'),
  ('David Thompson', 'A whimsical and colorful set inspired by a fairytale forest. The set features oversized flowers, enchanted trees, and a magical ambiance, creating a dreamlike atmosphere.', "David Thompson's set designs bring imagination to life, transporting audiences to new and exciting realms.");


INSERT INTO Genres (genre_name, description)
VALUES
  ('Drama', 'Theater performances that explore intense emotions and human relationships.'),
  ('Comedy', 'Entertaining plays with humorous elements and comedic situations.'),
  ('Musical', 'Stage productions that incorporate singing, dancing, and music to tell a story.'),
  ('Tragedy', 'Serious and somber performances that often end in disaster or sorrow.'),
  ('Romance', 'Theater shows that focus on love, relationships, and emotional connections.');


INSERT INTO Spectacles (genre_id, director_id, costume_designer_id, set_designer_id, title, author, duration, description)
VALUES
  (1, 1, 1, 1, 'Hamlet', 'William Shakespeare', '02:30:00', 'A tragedy about a prince torn between duty and revenge.'),
  (2, 2, 2, 2, 'The Comedy of Errors', 'William Shakespeare', '02:00:00', 'A hilarious play about mistaken identities and humorous misunderstandings.'),
  (3, 3, 4, 3, 'Wicked', 'Stephen Schwartz', '02:45:00', 'A musical prequel to "The Wizard of Oz" that tells the untold story of the Wicked Witch of the West.'),
  (1, 5, 1, 2, 'Romeo and Juliet', 'William Shakespeare', '02:30:00', 'A tragic love story of two young star-crossed lovers from feuding families.'),
  (3, 4, 3, 4, 'The Phantom of the Opera', 'Andrew Lloyd Webber', '02:45:00', 'A haunting tale of love and obsession set in the Paris Opera House.');



INSERT INTO Roles (actor_id, spectacle_id, role)
VALUES
  (1, 1, 'Hamlet'),
  (2, 1, 'Ophelia'),
  (3, 2, 'Jack'),
  (4, 2, 'Jill'),
  (5, 3, 'Elphaba');


INSERT INTO Scenes (scene_name, seats_amount, address, contacts)
VALUES
  ('Main Theater', 500, '123 Main Street', 'maintheater@example.com'),
  ('Studio Theater', 100, '456 Elm Street', 'studiotheater@example.com'),
  ('Black Box Theater', 50, '789 Oak Street', 'blackboxtheater@example.com'),
  ('Outdoor Amphitheater', 300, '321 Maple Street', 'outdoortheater@example.com'),
  ('Experimental Space', 80, '654 Pine Street', 'experimentalspace@example.com');


INSERT INTO Schedule (spectacle_id, scene_id, date, start_time)
VALUES
  (1, 1, '2023-06-01', '19:30:00'),
  (2, 2, '2023-06-05', '20:00:00'),
  (3, 3, '2023-06-10', '18:00:00'),
  (4, 4, '2023-06-15', '19:30:00'),
  (5, 5, '2023-06-20', '20:00:00');



INSERT INTO Discounts (discount_name, discount_percent, start_date, end_date)
VALUES
  ('New User Discount', 10.00, '2023-01-01', '2023-12-31'),
  ('Summer Sale', 20.00, '2023-06-01', '2023-08-31'),
  ('Holiday Special', 15.00, '2023-12-15', '2023-12-31');



INSERT INTO Users (discount_id, full_name, phone_number, email, status)
VALUES
  (1, 'John Smith', '+1234567890', 'john@example.com', 'Active'),
  (2, 'Emily Johnson', '+0987654321', 'emily@example.com', 'Active'),
  (NULL, 'Michael Davis', '+1112223333', 'michael@example.com', 'Inactive'),
  (3, 'Sophia Wilson', '+4445556666', 'sophia@example.com', 'Active'),
  (NULL, 'Daniel Thompson', '+7778889999', 'daniel@example.com', 'Active');




INSERT INTO Places (scene_id, view, `row`, seat, status)
VALUES
  (1, 'Balcony', 1, 1, 'Occupied'),
  (1, 'Balcony', 1, 2, 'Available'),
  (1, 'Balcony', 1, 3, 'Available'),
  (1, 'Orchestra', 2, 1, 'Available'),
  (1, 'Orchestra', 2, 2, 'Occupied'),
  (1, 'Orchestra', 2, 3, 'Available'),
  (2, 'General', 1, 1, 'Available'),
  (2, 'General', 1, 2, 'Available'),
  (2, 'General', 1, 3, 'Occupied'),
  (2, 'General', 2, 1, 'Available'),
  (2, 'General', 2, 2, 'Available'),
  (2, 'General', 2, 3, 'Available'),
  (3, 'Open Seating', 1, 1, 'Available'),
  (3, 'Open Seating', 1, 2, 'Available'),
  (3, 'Open Seating', 1, 3, 'Occupied');


INSERT INTO Tickets (spectacle_id, scene_id, date, start_time, view, `row`, seat, price)
VALUES
  (1, 1, '2023-06-01', '19:30:00', 'Balcony', 1, 1, 50.00),
  (1, 1, '2023-06-01', '19:30:00', 'Balcony', 1, 2, 50.00),
  (1, 1, '2023-06-01', '19:30:00', 'Balcony', 1, 3, 50.00),
  (1, 1, '2023-06-01', '19:30:00', 'Orchestra', 2, 1, 80.00),
  (1, 1, '2023-06-01', '19:30:00', 'Orchestra', 2, 2, 80.00),
  (1, 1, '2023-06-01', '19:30:00', 'Orchestra', 2, 3, 80.00),
  (2, 2, '2023-06-05', '20:00:00', 'General', 1, 1, 40.00),
  (2, 2, '2023-06-05', '20:00:00', 'General', 1, 2, 40.00),
  (2, 2, '2023-06-05', '20:00:00', 'General', 1, 3, 40.00),
  (2, 2, '2023-06-05', '20:00:00', 'General', 2, 1, 40.00);



INSERT INTO Orders (ticket_id, user_id, order_date, `status`)
VALUES
  (1, 1, '2023-05-29 10:30:00', 'Confirmed'),
  (2, 2, '2023-05-29 15:45:00', 'Confirmed'),
  (3, 3, '2023-05-30 09:15:00', 'Pending'),
  (4, 1, '2023-05-30 14:20:00', 'Confirmed'),
  (5, 2, '2023-05-31 11:10:00', 'Pending');



INSERT INTO Payments (order_id, payment_method, status)
VALUES
  (1, 'Credit Card', 'Completed'),
  (2, 'PayPal', 'Completed'),
  (3, 'Cash', 'Pending'),
  (4, 'Credit Card', 'Completed'),
  (5, 'PayPal', 'Pending');



INSERT INTO Reviews (spectacle_id, user_id, review_date, review_text, rating)
VALUES
  (1, 1, '2023-06-02', 'The performance was outstanding. The actors delivered powerful performances, and the set design was breathtaking. I highly recommend this spectacle!', 5),
  (1, 2, '2023-06-02', 'I was captivated from beginning to end. The costumes were exquisite, and the direction was flawless. Truly a masterpiece!', 5),
  (2, 3, '2023-06-06', 'A hilarious comedy that had the audience laughing non-stop. The actors had incredible comedic timing, and the script was brilliant. I had a fantastic time!', 4),
  (3, 4, '2023-06-10', 'The musical was a feast for the senses. The songs were catchy, the choreography was mesmerizing, and the performances were top-notch. I left the theater singing and dancing!', 5),
  (4, 1, '2023-06-12', 'A tragic tale that left me in tears. The actors portrayed their characters with raw emotion, and the set design created a haunting atmosphere. It was a deeply moving experience.', 4);
