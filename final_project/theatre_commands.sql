Создание:

Создание нового спектакля в таблице Spectacles:
INSERT INTO Spectacles (genre_id, director_id, costume_designer_id, set_designer_id, title, author, date_of_performance, duration, description)
VALUES (1, 1, 1, 1, 'Название спектакля', 'Автор спектакля', '2023-05-28', '02:30:00', 'Описание спектакля');

Создание нового актера в таблице Actors:
INSERT INTO Actors (full_name, bio, contacts)
VALUES ('Имя актера', 'Биография актера', 'Контактная информация актера');

Создание нового режиссера в таблице Directors:
INSERT INTO Directors (full_name, bio, main_production, contacts)
VALUES ('Имя режиссера', 'Биография режиссера', 'Работа режиссера', 'Контактная информация режиссера');

Создание нового костюмографа в таблице Costume_Designers:
INSERT INTO Costume_Designers (full_name, costume_example, bio)
VALUES ('Имя костюмографа', 'Пример костюма с описанием', 'Биография костюмографа');

Создание нового художника-декоратора в таблице Set_Designers:
INSERT INTO Set_Designers (full_name, set_example, bio)
VALUES ('Имя художника-декоратора', 'Пример декорации с описанием', 'Биография художника-декоратора');

Создание новой сцены в таблице Scenes:
INSERT INTO Scenes (scene_name, seats_amount, address, contacts)
VALUES ('Название сцены', 100, 'Адрес сцены', 'Контактная информация сцены');

Создание нового места в таблице Places:
INSERT INTO Places (scene_id, view, row, seat, status)
VALUES (1, 'General', 1, 1, 'Available');

Создание нового отзыва в таблице Reviews:
INSERT INTO Reviews (spectacle_id, user_id, review_date, review_text, rating)
VALUES (1, 1, '2023-06-01', 'Текст отзыва', 5);

Создание нового расписания в таблице Schedule:
INSERT INTO Schedule (spectacle_id, scene_id, date, start_time)
VALUES (1, 1, '2023-06-01', '19:00:00');

Создание новой роли в таблице Roles:
INSERT INTO Roles (actor_id, spectacle_id, role)
VALUES (1, 1, 'Главная роль');










Правка:

Изменение информации о спектакле в таблице Spectacles:
UPDATE Spectacles
SET title = 'Новое название спектакля'
WHERE id = 1;

Изменение информации об актере в таблице Actors:
UPDATE Actors
SET full_name = 'Новое имя актера'
WHERE id = 1;

Изменение информации о режиссере в таблице Directors:
UPDATE Directors
SET full_name = 'Новое имя режиссера'
WHERE id = 1;

Изменение информации о костюмографе в таблице Costume_Designers:
UPDATE Costume_Designers
SET full_name = 'Новое имя костюмографа'
WHERE id = 1;

Изменение информации о художнике-декораторе в таблице Set_Designers:
UPDATE Set_Designers
SET full_name = 'Новое имя художника-декоратора'
WHERE id = 1;

Изменение информации о сцене в таблице Scenes:
UPDATE Scenes
SET scene_name = 'Новое название сцены'
WHERE id = 1;

Изменение информации о месте в таблице Places:
UPDATE Places
SET view = 'Open Seating', row = 2, seat = 2, status = 'Occupied'
WHERE id = 1;

Изменение статуса пользователя в таблице Users:
UPDATE Users
SET status = 'Active'
WHERE id = 1;








Удаление:

Удаление спектакля из таблицы Spectacles:
DELETE FROM Spectacles
WHERE id = 1;

Удаление актера из таблицы Actors:
DELETE FROM Actors
WHERE id = 1;

Удаление режиссера из таблицы Directors:
DELETE FROM Directors
WHERE id = 1;

Удаление костюмографа из таблицы Costume_Designers:
DELETE FROM Costume_Designers
WHERE id = 1;

Удаление художника-декоратора из таблицы Set_Designers:
DELETE FROM Set_Designers
WHERE id = 1;

Удаление сцены из таблицы Scenes:
DELETE FROM Scenes
WHERE id = 1;

Удаление места из таблицы Places:
DELETE FROM Places
WHERE id = 1;







Заказ и покупка:

Создание нового заказа в таблице Orders:
INSERT INTO Orders (ticket_id, user_id, order_date, status)
VALUES (6, 5, CURRENT_TIMESTAMP, 'Confirmed');

Обновление статуса и мест после покупки:
UPDATE Places
SET status = 'Occupied'
WHERE id = 1;

Оплата заказа и обновление статуса платежа:
INSERT INTO Payments (order_id, payment_method, status)
VALUES (1, 'Credit Card', 'Completed');

Обновление статуса билетов и мест после возврата:
UPDATE Places
SET status = 'Available'
WHERE id = 1;













Выполнение и смена статусов:

Изменение статуса заказа на основе его обработки и выполнения:
UPDATE Orders
SET status = 'Confirmed'
WHERE id = 1;

Изменение статуса платежа на основе его обработки и выполнения:
UPDATE Payments
SET status = 'Completed'
WHERE order_id = 1;













