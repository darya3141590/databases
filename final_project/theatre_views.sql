Представление для просмотра информации о спектаклях и их режиссерах:

CREATE VIEW SpectacleDirectorView AS
SELECT s.id AS spectacle_id, s.title AS spectacle_title, d.full_name AS director_name
FROM Spectacles s
JOIN Directors d ON s.director_id = d.id;

Представление для просмотра информации о билетах, включая информацию о спектакле и сцене:

CREATE VIEW TicketInfoView AS
SELECT t.id AS ticket_id, t.date, t.start_time, s.title AS spectacle_title, sc.scene_name
FROM Tickets t
JOIN Spectacles s ON t.spectacle_id = s.id
JOIN Scenes sc ON t.scene_id = sc.id;

Представление для просмотра информации о заказах, включая информацию о пользователях и статусе заказа:

CREATE VIEW OrderInfoView AS
SELECT o.id AS order_id, u.full_name AS user_name, t.date, t.start_time, o.status
FROM Orders o
JOIN Users u ON o.user_id = u.id
JOIN Tickets t ON o.ticket_id = t.id;

Представление для просмотра информации о жанрах и количестве спектаклей в каждом жанре:

CREATE VIEW GenreStatsView AS
SELECT g.genre_name, COUNT(s.id) AS spectacle_count
FROM Genres g
LEFT JOIN Spectacles s ON g.id = s.genre_id
GROUP BY g.genre_name;

Представление для просмотра информации о местах в сценах, включая статус каждого места:

CREATE VIEW PlaceStatusView AS
SELECT p.id AS place_id, s.scene_name, p.view, p.row, p.seat, p.status
FROM Places p
JOIN Scenes s ON p.scene_id = s.id;

Представление для просмотра информации о пользователях, включая их статус и применяемую скидку:

CREATE VIEW UserDiscountView AS
SELECT u.id AS user_id, u.full_name, u.status, d.discount_name, d.discount_percent
FROM Users u
LEFT JOIN Discounts d ON u.discount_id = d.id;

Представление для просмотра информации о спектаклях, включая количество и средний рейтинг отзывов:

CREATE VIEW SpectacleRatingView AS
SELECT s.id AS spectacle_id, s.title AS spectacle_title, COUNT(r.spectacle_id) AS review_count, AVG(r.rating) AS average_rating
FROM Spectacles s
LEFT JOIN Reviews r ON s.id = r.spectacle_id
GROUP BY s.id;

Представление для просмотра информации о заказах и связанных с ними билетах, включая информацию о спектакле и пользователе:

CREATE VIEW OrderTicketView AS
SELECT o.id AS order_id, t.id AS ticket_id, s.title AS spectacle_title, u.full_name AS user_name, o.order_date, t.date, t.start_time, o.status
FROM Orders o
JOIN Tickets t ON o.ticket_id = t.id
JOIN Spectacles s ON t.spectacle_id = s.id
JOIN Users u ON o.user_id = u.id;

Представление для просмотра информации о доступных местах в сценах, исключая зарезервированные и проданные места:

CREATE VIEW AvailablePlacesView AS
SELECT p.id AS place_id, s.scene_name, p.view, p.row, p.seat
FROM Places p
JOIN Scenes s ON p.scene_id = s.id
WHERE p.status = 'Available';

Представление для просмотра информации о пользователях и связанных с ними заказах, включая суммарную стоимость заказов:

CREATE VIEW UserOrderView AS
SELECT u.id AS user_id, u.full_name, u.email, COUNT(o.id) AS order_count, SUM(t.price) AS total_amount
FROM Users u
LEFT JOIN Orders o ON o.user_id = u.id
LEFT JOIN Tickets t ON o.ticket_id = t.id
GROUP BY u.id;









