-- Задача 18
-- Рецепт (КодБлюда, КодПродукта, НазвБлюда, НазвПродукта, КолДанногоПродуктаВБлюде)`


CREATE TABLE IF NOT EXISTS `recipe` (
    `dish_code` varchar(255),
    `product_code` varchar(255),
    `dish_name` varchar(255),
    `product_name` varchar(255),
    `product_quantity` varchar(40)
);

INSERT INTO `recipe`(`dish_code`, `product_code`, `dish_name`, `product_name`, `product_quantity`) VALUES
('10.85.13.000', 'a4c10987-67d1-8dfe-7780-34b7269fce6d', 'рататуй', 'кабачок', '2 штуки'),
('10.85.13.000', 'gt4gewhh-74hr-474f-4839-4742h38rhye8', 'рататуй', 'красный болгарский перец', '2 штуки'),
('10.85.13.000', '36gwye83-de7e-733y-7389-gg3u47edu387', 'рататуй', 'баклажан', '1 штуки'),
('10.85.13.000', 'swt7whd8-h46e-yh37-5363-bgeg4728e38e', 'рататуй', 'лук', '2 штуки'),
('10.85.13.000', 'tgewy783-473g-dej4-2526-bsye3gt34e8y', 'рататуй', 'помидор', '4 штуки'),
('10.76.15.000', '84h3gwu8-j3u8-h3yh-6380-g574yhe3974y', 'паста с фетой', 'фета', '200-230 грамм'),
('10.76.15.000', 'tgewy783-473g-dej4-2526-bsye3gt34e8y', 'паста с фетой', 'помидор', '600 грамм'),
('10.76.15.000', '673gh7g3-734g-73eu-8463-bgewy372f63u', 'паста с фетой', 'чеснок', '2 зубчика'),
('10.45.76.001', 'swt7whd8-h46e-yh37-5363-bgeg4728e38e', 'буузы', 'лук', '1 головка'),
('10.45.76.001', '637hy7sh-73ge-73yh-8373-jwe63wet7328', 'буузы', 'говяжий фарш', '500 грамм'),
('10.45.76.001', '73gyhsy7-ghd2-846d-6484-vgs6623t23fr', 'буузы', 'вода', '300 миллилитров'),
('10.45.76.001', 'gts6638d-xfte-twe6-5433-xzfywe638n4d', 'буузы', 'пшеничная мука', '500 грамм'),
('32.26.13.012', '532rw673-dhs6-xgt2-6472-xrt62f2w5o8j', 'творожный кекс', 'творог', '200 грамм'),
('32.26.13.012', 'gts6638d-xfte-twe6-5433-xzfywe638n4d', 'творожный кекс', 'пшеничная мука', '1 стакан'),
('32.26.13.012', 'fgysd638-dg78-ewg3-0689-zf63ltriruy4', 'творожный кекс', 'сметана', '2 столовые ложки'),
('32.26.13.012', '5fgyds7o-cgy7-dfy2-2847-lhik59573h4u', 'творожный кекс', 'куриные яйца', '2 штуки');

CREATE TABLE IF NOT EXISTS `dishes` (
    `id` int unsigned AUTO_INCREMENT,
    `dish_code` varchar(255),
    `dish_name` varchar(255),
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `products` (
    `id` int unsigned AUTO_INCREMENT,
    `product_code` varchar(255),
    `product_name` varchar(255),
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `product_content` (
    `id` int unsigned AUTO_INCREMENT,
    `dish_id` int unsigned not null,
    `product_id` int unsigned not null,
    `product_quantity` varchar(40),
    PRIMARY KEY (`id`),
    CONSTRAINT FOREIGN KEY fk_dish_id(dish_id)
    REFERENCES `dishes`(id),
    CONSTRAINT FOREIGN KEY fk_product_id(product_id)
    REFERENCES `products`(id)
);

ALTER TABLE `dishes` ADD UNIQUE (dish_code);
ALTER TABLE `products` ADD UNIQUE (product_code);

INSERT IGNORE INTO `dishes`(dish_code, dish_name) SELECT dish_code, dish_name FROM `recipe`;
INSERT IGNORE INTO `products`(product_code, product_name) SELECT product_code, product_name FROM `recipe`;
INSERT IGNORE INTO `product_content`(dish_id, product_id, product_quantity) SELECT dishes.id, products.id, recipe.product_quantity FROM products LEFT JOIN recipe ON products.product_code = recipe.product_code RIGHT JOIN dishes ON recipe.dish_code = dishes.dish_code;