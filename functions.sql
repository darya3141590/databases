-- Задав DELIMITER равным //, мы получаем возможность использовать точку с запятой внутри кода например создание процедуры.
-- то есть Указав Delimiter // mysql не будет воспринимать ; ( точку с запятой) как окончание sql запроса и не будет пытаться выполнить его.
-- нужно не забывать переопределять его назад к стандарному ; чтобы потом точки с запятой снова воспринимались


DELIMITER //

CREATE FUNCTION is_anniversary(birth_date DATE) RETURNS INT
DETERMINISTIC -- без этой штуки у тебя не будет работать функция
BEGIN
    DECLARE today DATE; -- обьявление переменных
    DECLARE age INT;
    
    SET today = CURDATE(); -- сегодняшная дата
    SET age = YEAR(today) - YEAR(birth_date); -- определить возраст чела на данный год(если у него др допустим еще не исполнилось, все равно значение будет таким как будто исполнилось)
    
    IF age % 5 = 0 THEN -- определение юбилея, можешь 10 написать, если круглая дата
        RETURN age;
    ELSE
        RETURN NULL;
    END IF;
END //

DELIMITER ;

SELECT id, is_anniversary(birth_date) FROM salesmen; -- это вызов функции, туда передаю столбец из таблицы с др










DELIMITER //

CREATE FUNCTION fio(fio VARCHAR(255)) RETURNS VARCHAR(255)
DETERMINISTIC
BEGIN
    DECLARE last_name VARCHAR(255); -- обьявление фио
    DECLARE first_name VARCHAR(255);
    DECLARE middle_name VARCHAR(255);
    
    SET last_name = SUBSTRING_INDEX(fio, ' ', 1); --здесь будет из переданного значения выбрана фамилия полностью
    SET first_name = SUBSTRING_INDEX(SUBSTRING_INDEX(fio, ' ', -2), ' ', 1); -- тут имя
    SET middle_name = SUBSTRING_INDEX(fio, ' ', -1); --тут отчество
    
    IF last_name != '' AND first_name != '' AND middle_name != '' THEN --если они не пустые
        RETURN CONCAT(last_name, ' ', LEFT(first_name, 1), '.', LEFT(middle_name, 1), '.'); --соединяю в одну строку и беру первые буквы у имени и отчества
    ELSE
        RETURN '######';
    END IF;
END //

DELIMITER ;

SELECT id, fio(CONCAT_WS(' ', last_name, first_name, middle_name)) FROM salesmen; --передаю сразу фио, прост в таблице это все отдельные 3 столбца












DELIMITER //

CREATE FUNCTION calculate_salesman_income(id_of_salesman INT, sales DECIMAL(10,2)) RETURNS DECIMAL(10, 2)
DETERMINISTIC
BEGIN
    DECLARE salesman_rate DECIMAL(10, 2);
    DECLARE salesman_income DECIMAL(10, 2);

    SET salesman_rate = (SELECT rate --для указанного id чела ищу какая у него ставка в таблице
    FROM salesmen
    WHERE id = id_of_salesman LIMIT 1);

    SET salesman_income = salesman_rate * sales; --считаю доход
    RETURN salesman_income;
END //

DELIMITER ;

SELECT calculate_salesman_income(5, 67999); --передаю айдишник и какой у чела доход без учета ставки











DELIMITER //

CREATE FUNCTION calculate_income(cost DECIMAL(10, 2), quantity INT) RETURNS DECIMAL(10, 2)
DETERMINISTIC
BEGIN
    DECLARE income DECIMAL(10, 2);
    SET income = cost * quantity; --тут доход уже компании
    RETURN income;
END //

DELIMITER ;


SELECT calculate_income(4178, 6) AS income;











DELIMITER //

CREATE PROCEDURE list_anniversary_salesmen()
BEGIN
    DECLARE current_year INT;
    
    SET current_year = YEAR(CURDATE()); --текущий год
    
    SELECT last_name, first_name, middle_name, birth_date, 
           DATE_ADD(birth_date, INTERVAL (current_year - YEAR(birth_date)) YEAR) AS anniversary_date, --вот у тебя допустим у чела др 23.05.2016, а тебе надо дату когда у него юбилей будет в этом году, поэтому здесь мы просто меняем год на 2023
           TIMESTAMPDIFF(YEAR, birth_date, CURDATE()) AS age --возраст в годах
    FROM salesmen
    WHERE MOD(TIMESTAMPDIFF(YEAR, birth_date, CURDATE()), 5) = 0; --если юбилей, проверка на делимость
END //

DELIMITER ;

CALL list_anniversary_salesmen();











DELIMITER //

CREATE PROCEDURE get_products_by_group(IN group_id INT) --пишу in если передаю что-то в процедуру
BEGIN
    SELECT p.name AS good, g.name AS group_name, p.plu AS plu, p.cost AS cost, p.available AS available
    FROM products p
    INNER JOIN `groups` g ON p.group_id = g.id --groups в бэктиках так как иначе жалуется, там у sql есть команда чтоли такая
    WHERE p.group_id = group_id; --соединила таблички две для вывода инфы
END //

DELIMITER ;

CALL get_products_by_group(1);












DELIMITER //

CREATE PROCEDURE get_product_sales(IN product_name VARCHAR(255), IN days INT)
BEGIN
    SELECT s.date, CONCAT(smn.last_name, ' ', LEFT(smn.first_name, 1), '.', LEFT(smn.middle_name, 1), '.') AS fio
    FROM sales s
    INNER JOIN salesmen smn ON s.salesman_id = smn.id
    INNER JOIN products p ON s.product_id = p.id
    WHERE p.name COLLATE utf8mb4_unicode_ci = product_name COLLATE utf8mb4_unicode_ci AND s.date >= CURDATE() - INTERVAL days DAY AND s.date <= CURDATE(); --штука которая выводит продукт и дату проданного в течении заданного количества дней, начиная от сейчас даты
END // -- вот эта штука COLLATE utf8mb4_unicode_ci = product_name COLLATE utf8mb4_unicode_ci нужна чтоб не выдавало ошибку

DELIMITER ;

CALL get_product_sales('Мотоблок Oka MB-1D2M9', 7);



-- а это я проверяла если не от сейчас даты, так как в таблице даты не этого года


-- DELIMITER //

-- CREATE PROCEDURE get_product_sales_new(IN product_name VARCHAR(255), IN days INT)
-- BEGIN
--     SELECT s.date, CONCAT(smn.last_name, ' ', LEFT(smn.first_name, 1), '.', LEFT(smn.middle_name, 1), '.') AS fio
--     FROM sales s
--     INNER JOIN salesmen smn ON s.salesman_id = smn.id
--     INNER JOIN products p ON s.product_id = p.id
--     WHERE p.name COLLATE utf8mb4_unicode_ci = product_name COLLATE utf8mb4_unicode_ci AND s.date >= '2022-03-30' - INTERVAL days DAY AND s.date <= '2022-03-30';
-- END //

-- DELIMITER ;

-- CALL get_product_sales_new('Мотоблок Oka MB-1D2M9', 7);












DELIMITER //

CREATE PROCEDURE check_price_inconsistencies()
BEGIN
    DECLARE price_diff DECIMAL(10, 2);
    
    SELECT p.name AS product_name, s.cost AS declared_price, s.date AS sale_date, p.cost_changed_at AS cost_changed_at,
        p.cost AS current_price, (s.cost - p.cost) AS price_difference
    FROM sales s
    INNER JOIN products p ON s.product_id = p.id
    WHERE s.cost <> p.cost --если цена отличается, то есть изменилась, (если изменение цены произошло позднее даты продажи, такие данные не учитывать)
        AND s.date >= p.cost_changed_at
    ORDER BY s.date DESC;
    
    SET price_diff = FOUND_ROWS(); --считает скок таких строк нашлось
    
    IF price_diff = 0 THEN
        SELECT 'No price inconsistencies found.';
    END IF;
END //

DELIMITER ;

CALL check_price_inconsistencies();