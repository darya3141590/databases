CREATE TABLE price_change_info (
  id int unsigned NOT NULL AUTO_INCREMENT,
  product_id INT,
  change_date DATETIME,
  old_price DECIMAL(10, 2),
  new_price DECIMAL(10, 2),
  PRIMARY KEY (id)
);

DELIMITER //
CREATE TRIGGER trg_product_price_change
AFTER UPDATE ON products FOR EACH ROW
BEGIN
  IF NEW.cost <> OLD.cost THEN
    INSERT INTO price_change_info (product_id, change_date, old_price, new_price)
    VALUES (NEW.id, NOW(), OLD.cost, NEW.cost);
  END IF;
END//
DELIMITER ;



DELIMITER //

CREATE TRIGGER trg_delete_group
AFTER DELETE ON `groups`
FOR EACH ROW
BEGIN
  UPDATE products
  SET group_id = NULL, available = 0 
  WHERE group_id = OLD.id;
END//

DELIMITER ;
