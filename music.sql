
CREATE TABLE IF NOT EXISTS `music` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `profanity` enum('yes', 'no') DEFAULT NULL,
  `genre` varchar(20) NOT NULL,
  `author` varchar(255) NOT NULL,
  `language` varchar(50) NOT NULL,
  `duration` TIME NOT NULL,
  `album_name` varchar(255) NOT NULL,
  `release_date` date NOT NULL,
  `reproduced` int(5) unsigned NULL,
  `interesting_facts` TEXT,
  PRIMARY KEY (`id`)
);

INSERT INTO `music` (`id`, `name`, `profanity`, `genre`, `author`, `language`, `duration`, `album_name`, `release_date`, `reproduced`, `interesting_facts`) VALUES
(1, 'Flowers', 'no', 'pop', 'Miley Cyrus', 'english', '00:03:19', '', '2023-01-12', '15', 'The song has been described as a "revenge anthem" and "resonant" disco funk with a flamenco-like string section.'),
(2, 'The Astronaut', 'no', 'k-pop', 'JIN', 'english', '00:01:30', 'The Astronaut - Single', '2022-10-28', '2', 'The song is about Jins affection for and relationship with his fans. It explores themes of connection and love through the use of a recurring cosmic motif favored by both the band and singer, as evidenced in other songs they have written.'),
(3, 'Creepin', 'yes', 'hip-hop', 'Metro Boomin, The Weeknd & 21 Savage', 'english', '00:02:31', 'Todays Hits', '2022-12-13', '27', 'Jason Lipshutz at Billboard called the song a standout track on the album due to Metro engaging "in some rollicking genre exercises" and "joyfully" re-creating "I Dont Wanna Know".'),
(4, 'Calm Down', 'no', 'all', 'Rema', 'english', '00:03:40', '', '2022-02-11', '124', '"Calm Down" is about the events that led me to finding love at the time. It started at a party where I saw a girl who stood out from other girls so I felt like shooting my shot.'),
(5, 'Players', 'yes', 'hip-hop', 'Coi Leray', 'english', '00:02:34', '', '2022-11-30', '544', 'It contains samples of American hip hop group Grandmaster Flash and the Furious Fives 1982 single, "The Message", from their debut studio album The Message.'),
(6, 'Escapism.', 'yes', 'pop', 'RAYE & 070 Shake', 'english', '00:04:32', '', '2022-10-12', '2', 'The songs lyrics speak of escaping from reality and dealing with heartbreak.'),
(7, 'Kill Bill', 'no', 'R&B', 'SZA', 'english', '00:02:34', '', '2023-01-10', '75', 'The plot centers on an assassin named the Bride and her plans to murder Bill, the head of the enemy Deadly Viper assassins and a former love interest who tried to have her killed on the day she was about to be wed to someone else.'),
(8, 'Bloody Mary', 'no', 'pop', 'Lady Gaga', 'english', '00:04:05', 'Born This Way', '2022-12-02', '101', 'In 2022, following the release of the Netflix series Wednesday, the main characters dance and fan re-creations using the song went viral on TikTok.'),
(9, 'Painting Pictures', 'yes', 'hip-hop', 'Superstar Pride', 'english', '00:02:04', '5lbs of Pressure', '2022-10-22', '1', 'no interesting facts'),
(10, 'Miss You', 'yes', 'pop', 'Oliver Tree & Robin Schulz', 'english', '00:02:49', 'Party Bangers', '2022-05-09', '7', 'no interesting facts');
