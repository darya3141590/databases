1. a)SELECT id, UPPER(name) FROM `clients` LIMIT 10;
   b)SELECT id, LOWER(name) FROM `clients` LIMIT 10;
2. SELECT id, CONCAT_WS(' ', `name`, `lastname`) `full_name`, CONCAT(LEFT(`phone`, 3), REPEAT("*", CHAR_LENGTH(`phone`)-4), RIGHT(`phone`, 1)) `phone_number` FROM `clients` LIMIT 10;
3. SELECT id, CONCAT_WS(' ', CONCAT(LEFT(`name`, 1), '.'), `lastname`) `full_name` FROM `clients` WHERE INSTR(`lastname`, 'tt') OR INSTR(`lastname`, 'ss') OR INSTR(`lastname`, 'll') LIMIT 10;
4. 1 способ:
    a)SELECT `id`, `phone` FROM `clients` WHERE SUBSTRING(`phone` FROM 1 FOR 3)='586' LIMIT 10;
    b)SELECT `id`, `phone` FROM `clients` WHERE SUBSTRING(`phone` FROM 1 FOR 3)='657' OR SUBSTRING(`phone` FROM 2 FOR 3)='657' OR SUBSTRING(`phone` FROM 3 FOR 3)='657' OR SUBSTRING(`phone` FROM 4 FOR 3)='657' OR SUBSTRING(`phone` FROM 5 FOR 3)='657' LIMIT 10;
    c)SELECT `id`, `phone` FROM `clients` WHERE SUBSTRING(`phone` FROM -3 FOR 3)='707' LIMIT 10;
   2 способ:
    a)SELECT `id`, `phone` FROM `clients` WHERE LEFT(`phone`, 3)='586' LIMIT 10;
    c)SELECT `id`, `phone` FROM `clients` WHERE RIGHT(`phone`, 3)='707' LIMIT 10;
   3 способ:
    a)SELECT `id`, `phone` FROM `clients` WHERE POSITION('586' IN `phone`)=1 LIMIT 10;
    b)SELECT `id`, `phone` FROM `clients` WHERE POSITION('657' IN `phone`)!=0 LIMIT 10;
    c)SELECT `id`, `phone` FROM `clients` WHERE POSITION('707' IN `phone`)=5 LIMIT 10;
   4 способ:
    a)SELECT `id`, `phone` FROM `clients` WHERE LOCATE('586', `phone`)=1 LIMIT 10;
    b)SELECT `id`, `phone` FROM `clients` WHERE LOCATE('657', `phone`)!=0 LIMIT 10;
    c)SELECT `id`, `phone` FROM `clients` WHERE LOCATE('707', `phone`)=5 LIMIT 10;
   5 способ:
    a)SELECT `id`, `phone` FROM `clients` WHERE INSTR(`phone`, '586') AND POSITION('586' IN `phone`)=1 LIMIT 10;
    b)SELECT `id`, `phone` FROM `clients` WHERE INSTR(`phone`, '657') LIMIT 10;
    c)SELECT `id`, `phone` FROM `clients` WHERE INSTR(`phone`, '707') AND POSITION('707' IN `phone`)=5 LIMIT 10;


