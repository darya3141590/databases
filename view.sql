Создайте копию таблицы о расписании занятий (из прошлого задания), добавив в нее информацию о времени каждого занятия.
Заполните время по каждому предмету, предусмотрите возможные "окна" в расписании, не допускайте наложений одного предмета на другой.

CREATE TABLE subject_schedules_with_time AS
SELECT 
  ROW_NUMBER() OVER (ORDER BY s.date, s.subject_id) as id,
  s.subject_id, 
  s.date,
  ADDTIME('08:30:00', SEC_TO_TIME((ROW_NUMBER() OVER(PARTITION BY s.date ORDER BY s.subject_id)-1)*5400)) AS time_start,
  ADDTIME('08:30:00', SEC_TO_TIME((ROW_NUMBER() OVER(PARTITION BY s.date ORDER BY s.subject_id)-1)*5400 + 5400)) AS time_end
FROM 
  subject_schedules s
ORDER BY 
  s.date, s.subject_id;

UPDATE subject_schedules_with_time
SET time_start = ADDTIME(time_start, '00:10:00')
WHERE time_start > '08:30:00';











Создайте общее расписание занятий с учетом даты и времени, учитывая возможные "окна".

CREATE TABLE weekly_schedule AS
SELECT 
  ROW_NUMBER() OVER () AS id,
  s.id AS sub_id,
  DAYNAME(t.date) as day,
  t.time_start,
  t.time_end
FROM 
  subjects s
  JOIN subject_schedules_with_time t ON s.id = t.subject_id
WHERE 
  t.date BETWEEN '2016-09-08' AND '2016-09-12'
  AND NOT EXISTS (
    SELECT 1 
    FROM subject_schedules_with_time t2 
    WHERE 
      t2.date = t.date 
      AND t2.time_start < t.time_end 
      AND t.time_start < t2.time_end 
      AND t2.subject_id != t.subject_id
  )
ORDER BY 
  t.date, 
  t.time_start;









Создайте индивидуальное расписание занятий для каждого студента с учетом даты и времени.

CREATE TABLE individual_weekly_schedule AS
SELECT ROW_NUMBER() OVER (ORDER BY students.id, sub_id) AS id, students.id as stud_id, sub_id, day, time_start, time_end
FROM weekly_schedule
CROSS JOIN students
GROUP BY stud_id, sub_id, day, time_start, time_end
ORDER BY stud_id, sub_id;









Создайте представления для выше созданных расписаний.

CREATE VIEW weekly_schedule_view AS
SELECT
 sub.name subject, ws.day, CONCAT_WS('-', ws.time_start, ws.time_end) time
FROM weekly_schedule AS ws
JOIN subjects sub ON ws.sub_id = sub.id
GROUP BY subject, ws.day, time
ORDER BY subject;

CREATE VIEW individual_weekly_schedule_view AS
SELECT
  CONCAT(st.firstname, ' ', st.lastname) student,
  sub.name subject, ws.day, CONCAT_WS('-', ws.time_start, ws.time_end) time
FROM individual_weekly_schedule AS ws
JOIN subjects sub ON ws.sub_id = sub.id
JOIN students st ON ws.stud_id = st.id
GROUP BY student, subject, ws.day, time
ORDER BY student, subject;














Создайте представления для быстрого нахождения запросов из предыдущего задания.

CREATE VIEW student_average_marks_view AS
SELECT st.firstname, st.lastname, sub.name, AVG(m.mark) AS average_mark
FROM students AS st
JOIN student_marks AS m ON st.id = m.student_id
JOIN subjects AS sub ON sub.id = m.subject_id
GROUP BY sub.name, st.firstname, st.lastname;

CREATE VIEW student_mark_counts_view AS
SELECT st.firstname, st.lastname, m.mark, COUNT(m.mark) AS mark_count
FROM students AS st
JOIN student_marks AS m ON st.id = m.student_id
JOIN subjects AS sub ON sub.id = m.subject_id
GROUP BY st.firstname, st.lastname, m.mark
ORDER BY st.firstname, st.lastname, m.mark ASC;

CREATE VIEW top_average_student_view AS
SELECT *
FROM students
WHERE id = (
  SELECT student_id
  FROM student_marks
  GROUP BY student_id
  ORDER BY AVG(mark) DESC
  LIMIT 1
);

CREATE VIEW bottom_average_student_view AS
SELECT *
FROM students
WHERE id = (
  SELECT student_id
  FROM student_marks
  GROUP BY student_id
  ORDER BY AVG(mark) ASC
  LIMIT 1
);

CREATE VIEW top_average_subject_view AS
SELECT *
FROM subjects
WHERE id = (
  SELECT subject_id
  FROM student_marks
  GROUP BY subject_id
  ORDER BY AVG(mark) DESC
  LIMIT 1
);

CREATE VIEW daily_subjects_view AS
SELECT DAYNAME(date.date) AS day, GROUP_CONCAT(DISTINCT sub.name ORDER BY sub.name SEPARATOR ', ') AS subjects
FROM subject_schedules AS date
JOIN subjects AS sub ON sub.id = date.subject_id
GROUP BY day;

CREATE VIEW present_report AS
SELECT
p.present_date,
u.name subject,
CONCAT(t.firstname, ' ', t.lastname) student,
p.presence
FROM student_presents p
JOIN students t ON t.id = p.student_id
JOIN subjects u ON u.id = p.subject_id;










Сделайте несколько выборок из представлений, снабжая их условиями, группировками и сортировками.

SELECT firstname, lastname, mark, mark_count
FROM student_mark_counts_view
WHERE mark >= 3
ORDER BY mark DESC;

SELECT day, subjects
FROM daily_subjects_view
WHERE day = 'Monday';

SELECT *
FROM weekly_schedule_view
WHERE subject = 'Биология';

SELECT *
FROM individual_weekly_schedule_view
WHERE student = 'Сергей Иванов';