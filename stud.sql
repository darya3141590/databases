Упражнение 1:
1. SELECT `st`.`id`, `m`.`date`, `st`.`firstname`, `st`.`lastname`, `sub`.`name`, `m`.`mark` FROM `students` AS `st`, `student_marks` AS `m`, `subjects` AS `sub`
WHERE `st`.`id` = `m`.`student_id` AND `sub`.`id` = `m`.`subject_id`
ORDER BY `st`.`id` ASC;
2. SELECT `st`.`firstname`, `st`.`lastname`, `sub`.`name`, AVG(`m`.`mark`) FROM `students` AS `st`, `student_marks` AS `m`, `subjects` AS `sub`
WHERE `st`.`id` = `m`.`student_id` AND `sub`.`id` = `m`.`subject_id`
GROUP BY `sub`.`name`, `st`.`firstname`, `st`.`lastname`;
3. SELECT `st`.`firstname`, `st`.`lastname`, `m`.`mark`, COUNT(`m`.`mark`) FROM `students` AS `st`, `student_marks` AS `m`, `subjects` AS `sub`
WHERE `st`.`id` = `m`.`student_id` AND `sub`.`id` = `m`.`subject_id`
GROUP BY `st`.`firstname`, `st`.`lastname`, `m`.`mark`
ORDER BY `st`.`firstname`, `st`.`lastname`, `m`.`mark` ASC;
4. SELECT * FROM `students` WHERE `id` = (SELECT `student_id` FROM `student_marks` GROUP BY `student_id` ORDER BY AVG(`mark`) DESC LIMIT 1);
SELECT * FROM `students` WHERE `id` = (SELECT `student_id` FROM `student_marks` GROUP BY `student_id` ORDER BY AVG(`mark`) ASC LIMIT 1);
5. SELECT * FROM `subjects` WHERE `id` = (SELECT `subject_id`, FROM `student_marks` GROUP BY `subject_id` ORDER BY AVG(`mark`) DESC LIMIT 1);
6. SELECT DAYNAME(`date`.`date`) AS `day`, GROUP_CONCAT(DISTINCT `sub`.`name` ORDER BY `sub`.`name` SEPARATOR ', ') AS `subjects` FROM `subject_schedules` AS `date`, `subjects` AS `sub`
WHERE `sub`.`id` = `date`.`subject_id`
GROUP BY `day`;

Упражнение 2:
1. CREATE TABLE IF NOT EXISTS `student_presents` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`student_id` int(10) unsigned NOT NULL,
	`subject_id` int(10) unsigned NOT NULL,
	`present_date` date NOT NULL,
	`presence` BOOLEAN,
	PRIMARY KEY (`id`),
    KEY `student_id` (`student_id`),
    KEY `subject_id` (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
2. INSERT INTO `student_presents` (`student_id`, `subject_id`, `present_date`, `presence`) VALUES
('4', '6', '2023-04-13', '1'), 
('5', '8', '2023-04-11', '1'), 
('5', '4', '2023-04-07', '1'),
('5', '5', '2023-04-10', '1'), 
('5', '1', '2023-04-06', '1'), 
('2', '3', '2023-04-05', '1'), 
('3', '4', '2023-04-06', '1'), 
('4', '1', '2023-04-04', '0'),
('2', '3', '2023-04-07', '1'), 
('2', '6', '2023-04-10', '1'), 
('2', '2', '2023-04-06', '1'), 
('2', '6', '2023-04-11', '1'),  
('4', '2', '2023-04-03', '0'), 
('2', '7', '2023-04-11', '1'), 
('4', '3', '2023-04-07', '1'),
('1', '2', '2023-04-11', '0'),  
('2', '1', '2023-04-01', '1'), 
('3', '4', '2023-04-05', '1'), 
('2', '3', '2023-04-04', '1'), 
('3', '7', '2023-04-12', '1'), 
('2', '4', '2023-04-05', '0'), 
('1', '5', '2023-04-11', '1'), 
('5', '5', '2023-04-01', '1'), 
('2', '4', '2023-04-06', '0'), 
('4', '2', '2023-04-07', '0'), 
('2', '2', '2023-04-03', '1'), 
('1', '1', '2023-04-04', '0'), 
('3', '6', '2023-04-01', '1'), 
('4', '7', '2023-04-03', '1'), 
('2', '7', '2023-04-07', '1'), 
('4', '6', '2023-04-06', '0'), 
('1', '2', '2023-04-03', '0'), 
('3', '6', '2023-04-05', '1'), 
('3', '8', '2023-04-14', '0'), 
('5', '4', '2023-04-08', '1'), 
('4', '7', '2023-04-01', '0'), 
('5', '3', '2023-04-08', '1'), 
('3', '5', '2023-04-06', '1'), 
('4', '4', '2023-04-08', '1'), 
('3', '5', '2023-04-03', '0'), 
('5', '3', '2023-04-03', '0'), 
('4', '8', '2023-04-01', '1'), 
('4', '8', '2023-04-08', '1'), 
('1', '3', '2023-04-04', '1'), 
('3', '8', '2023-04-01', '1'), 
('3', '1', '2023-04-03', '0'), 
('1', '5', '2023-04-06', '0'), 
('2', '7', '2023-04-01', '1'), 
('1', '5', '2023-04-05', '0'), 
('3', '1', '2023-04-10', '0'), 
('1', '8', '2023-04-01', '0'), 
('3', '7', '2023-04-07', '0'), 
('3', '1', '2023-04-08', '0'), 
('1', '3', '2023-04-05', '1'), 
('4', '1', '2023-04-11', '1'), 
('1', '1', '2023-04-08', '1'), 
('5', '2', '2023-04-12', '1'), 
('4', '8', '2023-04-05', '1'),
('2', '4', '2023-04-03', '0'), 
('3', '3', '2023-04-01', '1'), 
('3', '6', '2023-04-07', '1'), 
('5', '2', '2023-04-14', '1'), 
('3', '7', '2023-04-08', '1'), 
('1', '2', '2023-04-05', '0'), 
('5', '3', '2023-04-07', '1'), 
('3', '7', '2023-04-11', '1'), 
('4', '6', '2023-04-12', '1'), 
('2', '5', '2023-04-01', '1'), 
('5', '1', '2023-04-05', '0'), 
('2', '2', '2023-04-13', '0'), 
('2', '4', '2023-04-12', '1'), 
('1', '2', '2023-04-10', '1'), 
('1', '3', '2023-04-01', '1'), 
('2', '3', '2023-04-12', '1'), 
('3', '3', '2023-04-03', '1'), 
('1', '1', '2023-04-10', '1'), 
('1', '3', '2023-04-13', '1'), 
('1', '1', '2023-04-05', '1'), 
('1', '5', '2023-04-07', '0'), 
('1', '8', '2023-04-04', '0'), 
('5', '7', '2023-04-01', '1'), 
('1', '6', '2023-04-12', '1'), 
('2', '3', '2023-04-08', '1'), 
('4', '8', '2023-04-10', '1'),
('1', '2', '2023-04-01', '1'), 
('1', '3', '2023-04-03', '1'), 
('5', '6', '2023-04-04', '1');
3. SELECT `pr`.`present_date`, `st`.`firstname`, `st`.`lastname`, GROUP_CONCAT(CASE WHEN `pr`.`presence` = true THEN `sub`.`name` END SEPARATOR ', ') AS `presence`, GROUP_CONCAT(CASE WHEN `pr`.`presence` = false THEN `sub`.`name` END SEPARATOR ', ') AS `no_presence`
FROM `students` AS `st`, `student_presents` AS `pr`, `subjects` AS `sub`
WHERE `st`.`id` = `pr`.`student_id` AND `sub`.`id` = `pr`.`subject_id`
GROUP BY `pr`.`present_date`, `st`.`firstname`, `st`.`lastname`
ORDER BY `pr`.`present_date`, `st`.`firstname`, `st`.`lastname`;


