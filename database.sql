CREATE TABLE Theatres (
  id INT PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  address VARCHAR(255),
  city VARCHAR(255)
);

CREATE TABLE Spectacles (
  id INT PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  author VARCHAR(255) NOT NULL,
  director_id INT,
  dateOfPerformance DATE NOT NULL,
  duration TIME NOT NULL,
  genre_id INT,
  actors_id INT,
  description TEXT NOT NULL,
  costume_designer_id INT,
  set_designer_id INT,
  CONSTRAINT FK_Spectacles_Directors FOREIGN KEY (director_id) REFERENCES Directors(id) ON DELETE SET NULL,
  CONSTRAINT FK_Spectacles_Genres FOREIGN KEY (genre_id) REFERENCES Genres(id) ON DELETE SET NULL,
  CONSTRAINT FK_Spectacles_Actors FOREIGN KEY (actors_id) REFERENCES Actors(id) ON DELETE SET NULL,
  CONSTRAINT FK_Spectacles_CostumeDesigners FOREIGN KEY (costume_designer_id) REFERENCES Costume_Designers(id) ON DELETE SET NULL,
  CONSTRAINT FK_Spectacles_SetDesigners FOREIGN KEY (set_designer_id) REFERENCES Set_Designers(id) ON DELETE SET NULL
);

CREATE TABLE Actors (
  id INT PRIMARY KEY,
  full_name VARCHAR(255) NOT NULL,
  bio TEXT,
  contacts VARCHAR(255)
);

CREATE TABLE Directors (
  id INT PRIMARY KEY,
  full_name VARCHAR(255) NOT NULL,
  bio TEXT,
  productions TEXT,
  contacts VARCHAR(255)
);

CREATE TABLE Costume_Designers (
  id INT PRIMARY KEY,
  full_name VARCHAR(255) NOT NULL,
  costume_examples TEXT,
  bio TEXT
);

CREATE TABLE Set_Designers (
  id INT PRIMARY KEY,
  full_name VARCHAR(255) NOT NULL,
  set_examples TEXT,
  bio TEXT
);

CREATE TABLE Scenes (
  id INT PRIMARY KEY,
  scene_name VARCHAR(50),
  seats_amount INT,
  address VARCHAR(255),
  contacts VARCHAR(255)
);

CREATE TABLE Places (
  id INT PRIMARY KEY,
  scene_id INT,
  view VARCHAR(50),
  row INT,
  seat INT,
  status VARCHAR(20),
  CONSTRAINT FK_Places_Scenes FOREIGN KEY (scene_id) REFERENCES Scenes(id) ON DELETE CASCADE
);

CREATE TABLE Tickets (
  id INT PRIMARY KEY,
  spectacle_id INT,
  date DATE,
  time TIME,
  scene_id INT,
  view VARCHAR(50),
  row INT,
  seat INT,
  price DECIMAL(8, 2),
  CONSTRAINT FK_Tickets_Spectacles FOREIGN KEY (spectacle_id) REFERENCES Spectacles(id) ON DELETE CASCADE,
  CONSTRAINT FK_Tickets_Scenes FOREIGN KEY (scene_id) REFERENCES Scenes(id) ON DELETE SET NULL
);

CREATE TABLE Orders (
  id INT PRIMARY KEY,
  ticket_id INT,
  user_id INT,
  order_date TIMESTAMP,
  status VARCHAR(20),
  CONSTRAINT FK_Orders_Tickets FOREIGN KEY (ticket_id) REFERENCES Tickets(id) ON DELETE CASCADE,
  CONSTRAINT FK_Orders_Users FOREIGN KEY (user_id) REFERENCES Users(id) ON DELETE SET NULL
);

CREATE TABLE Genres (
  id INT PRIMARY KEY,
  genre_name VARCHAR(50),
  description VARCHAR(255)
);

CREATE TABLE Discounts (
  id INT PRIMARY KEY,
  discount_name VARCHAR(255),
  discount_percent DECIMAL(8, 2),
  start_date DATE,
  end_date DATE
);

CREATE TABLE Payments (
  order_id INT,
  payment_method VARCHAR(50),
  status VARCHAR(20),
  CONSTRAINT FK_Payments_Orders FOREIGN KEY (order_id) REFERENCES Orders(id) ON DELETE CASCADE
);

CREATE TABLE Reviews (
  spectacle_id INT,
  user_id INT,
  review_date DATE,
  review_text TEXT,
  rating INT,
  CONSTRAINT FK_Reviews_Spectacles FOREIGN KEY (spectacle_id) REFERENCES Spectacles(id) ON DELETE CASCADE,
  CONSTRAINT FK_Reviews_Users FOREIGN KEY (user_id) REFERENCES Users(id) ON DELETE SET NULL
);

CREATE TABLE Schedule (
  id INT PRIMARY KEY,
  date DATE,
  time TIME,
  spectacle_id INT,
  scene_id INT,
  CONSTRAINT FK_Schedule_Spectacles FOREIGN KEY (spectacle_id) REFERENCES Spectacles(id) ON DELETE CASCADE,
  CONSTRAINT FK_Schedule_Scenes FOREIGN KEY (scene_id) REFERENCES Scenes(id) ON DELETE SET NULL
);

CREATE TABLE Users (
  id INT PRIMARY KEY,
  full_name VARCHAR(255),
  phone_number VARCHAR(20),
  email VARCHAR(255),
  status VARCHAR(20),
  discount_id INT,
  CONSTRAINT FK_Users_Discounts FOREIGN KEY (discount_id) REFERENCES Discounts(id) ON DELETE SET NULL
);

CREATE TABLE Roles (
  id INT PRIMARY KEY,
  actor_id INT,
  spectacle_id INT,
  role VARCHAR(20),
  CONSTRAINT FK_Roles_Actors FOREIGN KEY (actor_id) REFERENCES Actors(id) ON DELETE CASCADE,
  CONSTRAINT FK_Roles_Spectacles FOREIGN KEY (spectacle_id) REFERENCES Spectacles(id) ON DELETE CASCADE
);







INSERT INTO Actors (id, full_name, bio, contacts)
VALUES
  (1, 'John Smith', 'John Smith is a talented actor with a versatile range of performances.', 'johnsmith@example.com'),
  (2, 'Emma Johnson', 'Emma Johnson is an award-winning actress known for her powerful portrayals.', 'emmajohnson@example.com'),
  (3, 'Michael Brown', 'Michael Brown has years of experience in theater and has worked in various stage productions.', 'michaelbrown@example.com'),
  (4, 'Sophia Davis', 'Sophia Davis is a rising star in the acting industry, known for her charisma and stage presence.', 'sophiadavis@example.com'),
  (5, 'Robert Wilson', 'Robert Wilson is a seasoned actor with a deep understanding of character development.', 'robertwilson@example.com'),
  (6, 'Olivia Thompson', 'Olivia Thompson is a versatile actress who excels in both comedic and dramatic roles.', 'oliviathompson@example.com'),
  (7, 'David Rodriguez', 'David Rodriguez is known for his intense and emotionally charged performances on stage.', 'davidrodriguez@example.com'),
  (8, 'Sophie Anderson', 'Sophie Anderson brings grace and elegance to every character she portrays.', 'sophieanderson@example.com'),
  (9, 'Matthew Lee', 'Matthew Lee is a skilled actor with a knack for physical comedy.', 'matthewlee@example.com'),
  (10, 'Emily Moore', 'Emily Moore captivates audiences with her powerful voice and stage presence.', 'emilymoore@example.com'),
  (11, 'Daniel Harris', 'Daniel Harris is a dedicated actor who brings depth and authenticity to his roles.', 'danielharris@example.com');

INSERT INTO Directors (id, full_name, bio, productions, contacts)
VALUES
  (1, 'Jennifer Adams', 'Jennifer Adams is an accomplished director with a strong vision and innovative storytelling techniques.', 'Theater A, Theater B', 'jenniferadams@example.com'),
  (2, 'Michael Roberts', 'Michael Roberts has directed numerous critically acclaimed productions and is known for his attention to detail.', 'Theater C, Theater D', 'michaelroberts@example.com'),
  (3, 'Sarah Thompson', 'Sarah Thompson brings a unique and fresh perspective to her directorial work, pushing boundaries and challenging conventions.', 'Theater E, Theater F', 'sarahthompson@example.com'),
  (4, 'Jonathan Davis', 'Jonathan Davis is a visionary director who has a knack for creating visually stunning and emotionally impactful productions.', 'Theater G, Theater H', 'jonathandavis@example.com'),
  (5, 'Sophie Wilson', "Sophie Wilson's directing style is characterized by her ability to bring out authentic performances from actors.", 'Theater I, Theater J', 'sophiewilson@example.com'),
  (6, 'David Baker', 'David Baker is a versatile director who excels in both classic and contemporary theater productions.', 'Theater K, Theater L', 'davidbaker@example.com'),
  (7, 'Olivia Anderson', "Olivia Anderson's directorial work is known for its thought-provoking themes and innovative staging.", 'Theater M, Theater N', 'oliviaanderson@example.com'),
  (8, 'Daniel Harris', 'Daniel Harris brings a fresh and dynamic energy to his directing, creating memorable and impactful experiences for audiences.', 'Theater O, Theater P', 'danielharris@example.com'),
  (9, 'Emily Roberts', 'Emily Roberts is a highly sought-after director who has successfully brought classic works to life with a contemporary twist.', 'Theater Q, Theater R', 'emilyroberts@example.com'),
  (10, 'Matthew Wilson', "Matthew Wilson's directing style is marked by his ability to create intimate and emotionally charged productions.", 'Theater S, Theater T', 'matthewwilson@example.com');



INSERT INTO Costume_Designers (id, full_name, costume_examples, bio)
VALUES
  (1, 'Rachel Smith', 'Elegant Victorian dress with lace details. The costume is made of silk and features intricate embroidery on the bodice. It reflects the fashion of the 19th century.', 'Rachel Smith is a talented costume designer with a keen eye for detail and a passion for historical accuracy.'),
  (2, 'Michael Johnson', 'Colorful and vibrant costume inspired by traditional folk attire. It features intricate patterns and embellishments, showcasing the rich cultural heritage of the region.', 'Michael Johnson has designed costumes for numerous theater productions, bringing characters to life through his creative designs.'),
  (3, 'Sophie Davis', 'Contemporary costume with a futuristic twist. The design combines modern materials and unconventional silhouettes to create a bold and avant-garde look.', 'Sophie Davis combines her love for fashion and theater to create unique and visually stunning costumes for the stage.'),
  (4, 'Daniel Wilson', 'Elaborate period costume from the Renaissance era. The design incorporates luxurious fabrics, such as velvet and brocade, and intricate embellishments, reflecting the opulence of the time.', "Daniel Wilson's costume designs are known for their intricate details and ability to capture the essence of each character."),
  (5, 'Olivia Thompson', 'Sleek and elegant costume inspired by the fashion of the 1920s. The design features Art Deco motifs, fringe details, and a drop-waist silhouette, capturing the glamour of the era.', "Olivia Thompson's costume designs reflect her deep understanding of character psychology and storytelling.");


INSERT INTO Set_Designers (id, full_name, set_examples, bio)
VALUES
  (1, 'John Adams', 'A grand ballroom set with towering columns and a sparkling chandelier. The set evokes a sense of opulence and sets the stage for a glamorous event.', 'John Adams is a talented set designer with a knack for creating immersive and visually stunning stage environments.'),
  (2, 'Emma Wilson', 'A cozy and intimate living room set with a fireplace, comfortable furniture, and warm lighting. The set creates a homely atmosphere and is perfect for intimate scenes.', "Emma Wilson's set designs combine creativity and practicality to transform the stage into captivating worlds."),
  (3, 'Michael Davis', 'A futuristic cityscape set with towering skyscrapers, holographic billboards, and advanced technology. The set transports the audience into a sci-fi world of the future.', "Michael Davis's set designs are characterized by their innovative use of space and ability to enhance storytelling."),
  (4, 'Sophie Roberts', 'A versatile and modular set consisting of movable platforms and rotating panels. The set can be rearranged to create different locations, providing flexibility for various scenes.', 'Sophie Roberts creates sets that seamlessly blend aesthetics with functionality, providing a dynamic backdrop for performances.'),
  (5, 'David Thompson', 'A whimsical and colorful set inspired by a fairytale forest. The set features oversized flowers, enchanted trees, and a magical ambiance, creating a dreamlike atmosphere.', "David Thompson's set designs bring imagination to life, transporting audiences to new and exciting realms.");

