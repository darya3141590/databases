1 упражнение:
1. а)SELECT COUNT(*) `clients_quantity` FROM `clients`;

    +------------------+
    | clients_quantity |
    +------------------+
    |              378 |
    +------------------+

   б)SELECT COUNT(*) `older_1990` FROM `clients` WHERE YEAR(dbirth)>1990;

    +------------+
    | older_1990 |
    +------------+
    |         44 |
    +------------+

   в)SELECT name, COUNT(*) AS `count` FROM `clients`
   WHERE name in ('Thomas', 'Barbara', 'Willie')
   GROUP BY name;

    +---------+-------+
    | name    | count |
    +---------+-------+
    | Barbara |     2 |
    | Thomas  |     5 |
    | Willie  |     1 |
    +---------+-------+
2. а)SELECT YEAR(dbirth) AS `year`, COUNT(*) AS `count`
   FROM `clients`
   GROUP BY `year`
   ORDER BY `year`;

   +------+-------+
    | year | count |
    +------+-------+
    | 1930 |     6 |
    | 1931 |     1 |
    | 1932 |     6 |
    | 1933 |     3 |
    | 1934 |     5 |
    | 1935 |     9 |
    ...
    69 rows in set

   б)SELECT YEAR(dbirth) AS `year`, gender, COUNT(*) AS `count`
   FROM `clients`
   GROUP BY `year`, `gender`
   ORDER BY `year`;

   +------+--------+-------+
    | year | gender | count |
    +------+--------+-------+
    | 1930 | M      |     5 |
    | 1930 | F      |     1 |
    | 1931 | M      |     1 |
    | 1932 | M      |     3 |
    | 1932 | F      |     3 |
    | 1933 | M      |     1 |
    ...
    130 rows in set

3. SELECT MONTH(dbirth) AS `month`, COUNT(*) AS `count`
FROM `clients`
GROUP BY `month`
ORDER BY `month`;

+-------+-------+
| month | count |
+-------+-------+
|     1 |    33 |
|     2 |    28 |
|     3 |    31 |
|     4 |    36 |
|     5 |    39 |
|     6 |    27 |
|     7 |    36 |
|     8 |    37 |
|     9 |    27 |
|    10 |    27 |
|    11 |    31 |
|    12 |    26 |
+-------+-------+

4. SELECT MIN(TIMESTAMPDIFF(YEAR, dbirth, now())) `min`, MAX(TIMESTAMPDIFF(YEAR, dbirth, now())) `max`, AVG(TIMESTAMPDIFF(YEAR, dbirth, now())) `avg` FROM `clients`;

+------+------+---------+
| min  | max  | avg     |
+------+------+---------+
|   23 |   93 | 56.9868 |
+------+------+---------+

5. SELECT CONCAT(LEFT(YEAR(dbirth), 3), 'x') `years`, GROUP_CONCAT(DISTINCT name) `clients_name`
FROM `clients`
WHERE YEAR(dbirth) BETWEEN 1960 AND 1999
GROUP BY `years`;

+-------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| years | clients_name                                                                                                                                                                                                                                                                                                                                                                                                      |
+-------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 196x  | Ahti,Alan,Alec,Amos,Arthur,Barbara,Bennie,Betty,Beverly,Bryan,Carrie,Cheryl,Christopher,Clark,Clinton,David,Donna,Douglas,Elidia,Elizabeth,Enid,Ethel,Francine,Irene,Jeff,Jeffrey,Jim,John,Jonas,Joseph,Juri,Kenneth,Kevin,Kristel,Kurt,Lewis,Linda,Loretta,Michael,Michelle,Nicholas,Olive,Phyllis,Ronald,Rosemary,Ruth,Sharon,Sirje,Sten,Steven,Susan,Terry,Thomas,Walter                                       |
| 197x  | Allison,Amy,Anne,Austin,Brandon,Carol,Charles,Christine,Christopher,Clarissa,Cora,Corinna,Daniel,Dennis,Devin,Dianne,Eddie,Erica,Ester,Ethel,Inge,Jan,Janet,Janice,Jean,Jeffrey,Jennifer,Jessica,Jesus,Kaarel,Kaja,Karen,Kati,Kellie,Kelly,Krista,Lawrence,Marcus,Mari,Maria,Mart,Michael,Micheal,Nancy,Nicholas,Patrick,Peep,Priit,Randy,Rick,Ricky,Robert,Ruby,Scott,Shirley,Siret,Tammy,Tara,Terry,Tom,Whitney |
| 198x  | Alan,Arthur,Beverly,Brandi,Carly,Charles,Chase,Cindy,David,Dawn,Donna,Douglas,Eric,Frank,Glenda,Guillermo,Humberto,Irving,Janet,Jason,Jennifer,Jerry,Jesus,Joan,Jodie,John,Kimberly,Korey,Linda,Lucile,Lupe,Mary,Michael,Miranda,Ray,Raymond,Renae,Richard,Rick,Robert,Ronald,Rosemary,Sam,Samuel,Sandra,Stephen,Steven,Thomas,Tom,Tomasa,Willie,Zenaida                                                          |
| 199x  | Alphonso,Anthony,Betsy,Booker,Brandy,Carl,Christian,Christine,Clarice,Connie,Curtis,Deborah,Edward,Elise,Elsie,Emily,Evelyn,Fred,George,Goldie,James,Janet,Jeffrey,Jodie,Johanna,Jonathan,Jose,Keith,Laura,Lowell,Marie,Merlin,Morgan,Peggy,Penny,Rae,Raymond,Reina,Richard,Robert,Salvatore,Sherrie,Stephen,Tom                                                                                                  |
+-------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

6. SELECT CONCAT(LEFT(YEAR(dbirth), 3), 'x') `years`,  gender, COUNT(*) AS `count`
FROM `clients`
WHERE YEAR(dbirth) BETWEEN 1940 AND 1979
GROUP BY `years`, `gender`
ORDER BY `years`;

+-------+--------+-------+
| years | gender | count |
+-------+--------+-------+
| 194x  | M      |    26 |
| 194x  | F      |    18 |
| 195x  | M      |    28 |
| 195x  | F      |    27 |
| 196x  | M      |    39 |
| 196x  | F      |    24 |
| 197x  | M      |    30 |
| 197x  | F      |    34 |
+-------+--------+-------+

2 упражнение:
2. а)SELECT DAY(date) AS `day`, SUM(hits) AS `sum_hits`, SUM(loads) AS `sum_loads`
   FROM `stats`
   GROUP BY `day`
   ORDER BY `day`;
   б)SELECT country, SUM(hits) AS `sum_hits`, SUM(loads) AS `sum_loads`
   FROM `stats`
   GROUP BY country
   ORDER BY country;
   в)SELECT DAY(date) AS `day`, country, SUM(hits) AS `sum_hits`, SUM(loads) AS `sum_loads`
   FROM `stats`
   GROUP BY `day`, country
   ORDER BY `day`, country;
3. а)SELECT DAY(date) AS `day`, AVG(hits) AS `avg_hits`
   FROM `stats`
   GROUP BY `day`
   ORDER BY `day`;
   б)SELECT country, AVG(hits) AS `avg_hits`
   FROM `stats`
   GROUP BY country
   ORDER BY country;
4. а)SELECT DAY(date) AS `day`, AVG(loads) AS `avg_loads`
   FROM `stats`
   GROUP BY `day`
   ORDER BY `day`;
   б)SELECT country, AVG(loads) AS `avg_loads`
   FROM `stats`
   GROUP BY country
   ORDER BY country;
5. а)SELECT DAY(date) AS `day`, MIN(loads) AS `min_loads`, MAX(loads) AS `max_loads` 
   FROM `stats`
   GROUP BY `day`
   ORDER BY `day`;
   б)SELECT country, MIN(loads) AS `min_loads`, MAX(loads) AS `max_loads` 
   FROM `stats`
   GROUP BY country
   ORDER BY country;
6. SELECT DAY(date) AS `day`, CAST(SUM(hits) AS FLOAT) / CAST(SUM(loads) AS FLOAT) AS `result`
   FROM `stats`
   GROUP BY `day`
   ORDER BY `day`;
7. SELECT DAY(date) AS `day`, CAST(SUM(hits) AS FLOAT) / CAST(SUM(loads) AS FLOAT) AS `result`
   FROM `stats`
   GROUP BY `day`
   ORDER BY `result` DESC
   LIMIT 1;
8. SELECT country, CAST(SUM(hits) AS FLOAT) / CAST(SUM(loads) AS FLOAT) AS `result`
   FROM `stats`
   GROUP BY country
   ORDER BY `result` DESC
   LIMIT 5;