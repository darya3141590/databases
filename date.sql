1. SELECT id, YEAR(`dbirth`) `dbirth` FROM `clients` LIMIT 10;
2. a)SELECT id, TIMESTAMPDIFF(YEAR, `dbirth`, CURDATE()) `age` FROM `clients` LIMIT 10;
   b)SELECT id, TIMESTAMPDIFF(YEAR, `dbirth`, CONCAT(YEAR(CURDATE()), '-12-31')) `age` FROM `clients` LIMIT 10;
3. a)SELECT id, TIMESTAMPDIFF(YEAR, `dbirth`, '1992-12-08') `age` FROM `clients` LIMIT 10;
   b)SELECT id, TIMESTAMPDIFF(YEAR, `dbirth`, '2024-07-15') `age` FROM `clients` LIMIT 10;
4. a)SELECT id, DAY(`dbirth`)-DAY(CURDATE()) `day_to_birth` FROM `clients` WHERE WEEK(`dbirth`)=WEEK(CURDATE()) AND DAY(`dbirth`)-DAY(CURDATE()) BETWEEN 0 AND 6 LIMIT 10;
   b)SELECT id, DAY(`dbirth`)-DAY(CURDATE()) `day_to_birth` FROM `clients` WHERE WEEK(`dbirth`)-WEEK(CURDATE()) BETWEEN 0 AND 1 AND DAY(`dbirth`)-DAY(CURDATE()) BETWEEN 0 AND 13 LIMIT 10;
   c)SELECT id, DAY(`dbirth`)-DAY(CURDATE()) `day_to_birth` FROM `clients` WHERE MONTH(`dbirth`)=MONTH(CURDATE()) AND DAY(`dbirth`)-DAY(CURDATE())>=0 LIMIT 10;
